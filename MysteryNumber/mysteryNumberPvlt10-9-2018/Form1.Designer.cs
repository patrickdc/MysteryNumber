﻿namespace mysteryNumberPvlt10_9_2018
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chboxExtraPvlt = new System.Windows.Forms.CheckBox();
            this.chboxCheatPvlt = new System.Windows.Forms.CheckBox();
            this.btnStartPvlt = new System.Windows.Forms.Button();
            this.cmbAttemptsPvlt = new System.Windows.Forms.ComboBox();
            this.lblAttemptsPvlt = new System.Windows.Forms.Label();
            this.lblMaxValuePvlt = new System.Windows.Forms.Label();
            this.tbxMaxValuePvlt = new System.Windows.Forms.TextBox();
            this.lblMinValuePvlt = new System.Windows.Forms.Label();
            this.tbxMinValuePvlt = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnExtraExitPvlt = new System.Windows.Forms.Button();
            this.btnExtraRestartPvlt = new System.Windows.Forms.Button();
            this.btnExtraAboutPvlt = new System.Windows.Forms.Button();
            this.btnExtraLocatePvlt = new System.Windows.Forms.Button();
            this.btnExtraManualPvlt = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lblDifferenceValuePvlt = new System.Windows.Forms.Label();
            this.lblCheatNumber = new System.Windows.Forms.Label();
            this.lblDifferenceCheatPvlt = new System.Windows.Forms.Label();
            this.lblNumberCheatPvlt = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblGuessesLeftValuePvlt = new System.Windows.Forms.Label();
            this.lblPrevGuessValuePvlt = new System.Windows.Forms.Label();
            this.lblPreviousGuessesPvlt = new System.Windows.Forms.Label();
            this.pgbarGuessesPvlt = new System.Windows.Forms.ProgressBar();
            this.lblGuessesLeftPvlt = new System.Windows.Forms.Label();
            this.btnGuessPvlt = new System.Windows.Forms.Button();
            this.lblGuessPvlt = new System.Windows.Forms.Label();
            this.tbxMyGuessPvlt = new System.Windows.Forms.TextBox();
            this.tbarDifferencePvlt = new System.Windows.Forms.TrackBar();
            this.lblColdPvlt = new System.Windows.Forms.Label();
            this.lvlWarmPvlt = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lblLostValuePvlt = new System.Windows.Forms.Label();
            this.lblWonValuePvlt = new System.Windows.Forms.Label();
            this.lblLostPvlt = new System.Windows.Forms.Label();
            this.lblWonPvlt = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbarDifferencePvlt)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chboxExtraPvlt);
            this.groupBox1.Controls.Add(this.chboxCheatPvlt);
            this.groupBox1.Controls.Add(this.btnStartPvlt);
            this.groupBox1.Controls.Add(this.cmbAttemptsPvlt);
            this.groupBox1.Controls.Add(this.lblAttemptsPvlt);
            this.groupBox1.Controls.Add(this.lblMaxValuePvlt);
            this.groupBox1.Controls.Add(this.tbxMaxValuePvlt);
            this.groupBox1.Controls.Add(this.lblMinValuePvlt);
            this.groupBox1.Controls.Add(this.tbxMinValuePvlt);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(169, 172);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Settings";
            // 
            // chboxExtraPvlt
            // 
            this.chboxExtraPvlt.AutoSize = true;
            this.chboxExtraPvlt.Location = new System.Drawing.Point(112, 146);
            this.chboxExtraPvlt.Name = "chboxExtraPvlt";
            this.chboxExtraPvlt.Size = new System.Drawing.Size(50, 17);
            this.chboxExtraPvlt.TabIndex = 9;
            this.chboxExtraPvlt.Text = "Extra";
            this.chboxExtraPvlt.UseVisualStyleBackColor = true;
            this.chboxExtraPvlt.CheckedChanged += new System.EventHandler(this.chboxExtraPvlt_CheckedChanged);
            // 
            // chboxCheatPvlt
            // 
            this.chboxCheatPvlt.AutoSize = true;
            this.chboxCheatPvlt.Location = new System.Drawing.Point(7, 146);
            this.chboxCheatPvlt.Name = "chboxCheatPvlt";
            this.chboxCheatPvlt.Size = new System.Drawing.Size(54, 17);
            this.chboxCheatPvlt.TabIndex = 8;
            this.chboxCheatPvlt.Text = "Cheat";
            this.chboxCheatPvlt.UseVisualStyleBackColor = true;
            this.chboxCheatPvlt.CheckedChanged += new System.EventHandler(this.chboxCheatPvlt_CheckedChanged);
            // 
            // btnStartPvlt
            // 
            this.btnStartPvlt.Location = new System.Drawing.Point(7, 100);
            this.btnStartPvlt.Name = "btnStartPvlt";
            this.btnStartPvlt.Size = new System.Drawing.Size(155, 40);
            this.btnStartPvlt.TabIndex = 7;
            this.btnStartPvlt.Text = "Generate and Start!";
            this.btnStartPvlt.UseVisualStyleBackColor = true;
            this.btnStartPvlt.Click += new System.EventHandler(this.btnStartPvlt_Click);
            // 
            // cmbAttemptsPvlt
            // 
            this.cmbAttemptsPvlt.BackColor = System.Drawing.SystemColors.Window;
            this.cmbAttemptsPvlt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAttemptsPvlt.FormattingEnabled = true;
            this.cmbAttemptsPvlt.Items.AddRange(new object[] {
            "5",
            "10",
            "15",
            "20"});
            this.cmbAttemptsPvlt.Location = new System.Drawing.Point(7, 72);
            this.cmbAttemptsPvlt.Name = "cmbAttemptsPvlt";
            this.cmbAttemptsPvlt.Size = new System.Drawing.Size(101, 21);
            this.cmbAttemptsPvlt.TabIndex = 6;
            // 
            // lblAttemptsPvlt
            // 
            this.lblAttemptsPvlt.AutoSize = true;
            this.lblAttemptsPvlt.Location = new System.Drawing.Point(114, 75);
            this.lblAttemptsPvlt.Name = "lblAttemptsPvlt";
            this.lblAttemptsPvlt.Size = new System.Drawing.Size(48, 13);
            this.lblAttemptsPvlt.TabIndex = 5;
            this.lblAttemptsPvlt.Text = "Attempts";
            // 
            // lblMaxValuePvlt
            // 
            this.lblMaxValuePvlt.AutoSize = true;
            this.lblMaxValuePvlt.Location = new System.Drawing.Point(114, 49);
            this.lblMaxValuePvlt.Name = "lblMaxValuePvlt";
            this.lblMaxValuePvlt.Size = new System.Drawing.Size(42, 13);
            this.lblMaxValuePvlt.TabIndex = 3;
            this.lblMaxValuePvlt.Text = "Max nr.";
            // 
            // tbxMaxValuePvlt
            // 
            this.tbxMaxValuePvlt.Location = new System.Drawing.Point(7, 46);
            this.tbxMaxValuePvlt.Name = "tbxMaxValuePvlt";
            this.tbxMaxValuePvlt.Size = new System.Drawing.Size(100, 20);
            this.tbxMaxValuePvlt.TabIndex = 2;
            // 
            // lblMinValuePvlt
            // 
            this.lblMinValuePvlt.AutoSize = true;
            this.lblMinValuePvlt.Location = new System.Drawing.Point(114, 23);
            this.lblMinValuePvlt.Name = "lblMinValuePvlt";
            this.lblMinValuePvlt.Size = new System.Drawing.Size(39, 13);
            this.lblMinValuePvlt.TabIndex = 1;
            this.lblMinValuePvlt.Text = "Min nr.";
            // 
            // tbxMinValuePvlt
            // 
            this.tbxMinValuePvlt.Location = new System.Drawing.Point(7, 20);
            this.tbxMinValuePvlt.Name = "tbxMinValuePvlt";
            this.tbxMinValuePvlt.Size = new System.Drawing.Size(100, 20);
            this.tbxMinValuePvlt.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnExtraExitPvlt);
            this.groupBox3.Controls.Add(this.btnExtraRestartPvlt);
            this.groupBox3.Controls.Add(this.btnExtraAboutPvlt);
            this.groupBox3.Controls.Add(this.btnExtraLocatePvlt);
            this.groupBox3.Controls.Add(this.btnExtraManualPvlt);
            this.groupBox3.Location = new System.Drawing.Point(413, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(89, 172);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Extra";
            // 
            // btnExtraExitPvlt
            // 
            this.btnExtraExitPvlt.Location = new System.Drawing.Point(7, 136);
            this.btnExtraExitPvlt.Name = "btnExtraExitPvlt";
            this.btnExtraExitPvlt.Size = new System.Drawing.Size(75, 23);
            this.btnExtraExitPvlt.TabIndex = 4;
            this.btnExtraExitPvlt.Text = "Exit";
            this.btnExtraExitPvlt.UseVisualStyleBackColor = true;
            this.btnExtraExitPvlt.Click += new System.EventHandler(this.btnExtraExitPvlt_Click);
            // 
            // btnExtraRestartPvlt
            // 
            this.btnExtraRestartPvlt.Location = new System.Drawing.Point(7, 107);
            this.btnExtraRestartPvlt.Name = "btnExtraRestartPvlt";
            this.btnExtraRestartPvlt.Size = new System.Drawing.Size(75, 23);
            this.btnExtraRestartPvlt.TabIndex = 3;
            this.btnExtraRestartPvlt.Text = "Restart";
            this.btnExtraRestartPvlt.UseVisualStyleBackColor = true;
            this.btnExtraRestartPvlt.Click += new System.EventHandler(this.btnExtraRestartPvlt_Click);
            // 
            // btnExtraAboutPvlt
            // 
            this.btnExtraAboutPvlt.Location = new System.Drawing.Point(7, 78);
            this.btnExtraAboutPvlt.Name = "btnExtraAboutPvlt";
            this.btnExtraAboutPvlt.Size = new System.Drawing.Size(75, 23);
            this.btnExtraAboutPvlt.TabIndex = 2;
            this.btnExtraAboutPvlt.Text = "About";
            this.btnExtraAboutPvlt.UseVisualStyleBackColor = true;
            this.btnExtraAboutPvlt.Click += new System.EventHandler(this.btnExtraAboutPvlt_Click);
            // 
            // btnExtraLocatePvlt
            // 
            this.btnExtraLocatePvlt.Location = new System.Drawing.Point(7, 49);
            this.btnExtraLocatePvlt.Name = "btnExtraLocatePvlt";
            this.btnExtraLocatePvlt.Size = new System.Drawing.Size(75, 23);
            this.btnExtraLocatePvlt.TabIndex = 1;
            this.btnExtraLocatePvlt.Text = "Locate";
            this.btnExtraLocatePvlt.UseVisualStyleBackColor = true;
            this.btnExtraLocatePvlt.Click += new System.EventHandler(this.btnExtraLocatePvlt_Click);
            // 
            // btnExtraManualPvlt
            // 
            this.btnExtraManualPvlt.Location = new System.Drawing.Point(7, 20);
            this.btnExtraManualPvlt.Name = "btnExtraManualPvlt";
            this.btnExtraManualPvlt.Size = new System.Drawing.Size(75, 23);
            this.btnExtraManualPvlt.TabIndex = 0;
            this.btnExtraManualPvlt.Text = "Manual";
            this.btnExtraManualPvlt.UseVisualStyleBackColor = true;
            this.btnExtraManualPvlt.Click += new System.EventHandler(this.btnExtraManualPvlt_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lblDifferenceValuePvlt);
            this.groupBox4.Controls.Add(this.lblCheatNumber);
            this.groupBox4.Controls.Add(this.lblDifferenceCheatPvlt);
            this.groupBox4.Controls.Add(this.lblNumberCheatPvlt);
            this.groupBox4.Location = new System.Drawing.Point(12, 244);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(344, 47);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Cheat";
            // 
            // lblDifferenceValuePvlt
            // 
            this.lblDifferenceValuePvlt.AutoSize = true;
            this.lblDifferenceValuePvlt.Location = new System.Drawing.Point(237, 20);
            this.lblDifferenceValuePvlt.Name = "lblDifferenceValuePvlt";
            this.lblDifferenceValuePvlt.Size = new System.Drawing.Size(16, 13);
            this.lblDifferenceValuePvlt.TabIndex = 13;
            this.lblDifferenceValuePvlt.Text = "...";
            // 
            // lblCheatNumber
            // 
            this.lblCheatNumber.AutoSize = true;
            this.lblCheatNumber.Location = new System.Drawing.Point(98, 20);
            this.lblCheatNumber.Name = "lblCheatNumber";
            this.lblCheatNumber.Size = new System.Drawing.Size(16, 13);
            this.lblCheatNumber.TabIndex = 12;
            this.lblCheatNumber.Text = "...";
            // 
            // lblDifferenceCheatPvlt
            // 
            this.lblDifferenceCheatPvlt.AutoSize = true;
            this.lblDifferenceCheatPvlt.Location = new System.Drawing.Point(172, 20);
            this.lblDifferenceCheatPvlt.Name = "lblDifferenceCheatPvlt";
            this.lblDifferenceCheatPvlt.Size = new System.Drawing.Size(59, 13);
            this.lblDifferenceCheatPvlt.TabIndex = 11;
            this.lblDifferenceCheatPvlt.Text = "Difference:";
            // 
            // lblNumberCheatPvlt
            // 
            this.lblNumberCheatPvlt.AutoSize = true;
            this.lblNumberCheatPvlt.Location = new System.Drawing.Point(6, 20);
            this.lblNumberCheatPvlt.Name = "lblNumberCheatPvlt";
            this.lblNumberCheatPvlt.Size = new System.Drawing.Size(86, 13);
            this.lblNumberCheatPvlt.TabIndex = 10;
            this.lblNumberCheatPvlt.Text = "Mystery Number:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblGuessesLeftValuePvlt);
            this.groupBox2.Controls.Add(this.lblPrevGuessValuePvlt);
            this.groupBox2.Controls.Add(this.lblPreviousGuessesPvlt);
            this.groupBox2.Controls.Add(this.pgbarGuessesPvlt);
            this.groupBox2.Controls.Add(this.lblGuessesLeftPvlt);
            this.groupBox2.Controls.Add(this.btnGuessPvlt);
            this.groupBox2.Controls.Add(this.lblGuessPvlt);
            this.groupBox2.Controls.Add(this.tbxMyGuessPvlt);
            this.groupBox2.Location = new System.Drawing.Point(187, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(169, 172);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Game";
            // 
            // lblGuessesLeftValuePvlt
            // 
            this.lblGuessesLeftValuePvlt.AutoSize = true;
            this.lblGuessesLeftValuePvlt.Location = new System.Drawing.Point(80, 100);
            this.lblGuessesLeftValuePvlt.Name = "lblGuessesLeftValuePvlt";
            this.lblGuessesLeftValuePvlt.Size = new System.Drawing.Size(16, 13);
            this.lblGuessesLeftValuePvlt.TabIndex = 15;
            this.lblGuessesLeftValuePvlt.Text = "...";
            // 
            // lblPrevGuessValuePvlt
            // 
            this.lblPrevGuessValuePvlt.AutoSize = true;
            this.lblPrevGuessValuePvlt.Location = new System.Drawing.Point(89, 146);
            this.lblPrevGuessValuePvlt.Name = "lblPrevGuessValuePvlt";
            this.lblPrevGuessValuePvlt.Size = new System.Drawing.Size(16, 13);
            this.lblPrevGuessValuePvlt.TabIndex = 14;
            this.lblPrevGuessValuePvlt.Text = "...";
            // 
            // lblPreviousGuessesPvlt
            // 
            this.lblPreviousGuessesPvlt.AutoSize = true;
            this.lblPreviousGuessesPvlt.Location = new System.Drawing.Point(6, 146);
            this.lblPreviousGuessesPvlt.Name = "lblPreviousGuessesPvlt";
            this.lblPreviousGuessesPvlt.Size = new System.Drawing.Size(82, 13);
            this.lblPreviousGuessesPvlt.TabIndex = 12;
            this.lblPreviousGuessesPvlt.Text = "Previous guess:";
            // 
            // pgbarGuessesPvlt
            // 
            this.pgbarGuessesPvlt.Location = new System.Drawing.Point(9, 116);
            this.pgbarGuessesPvlt.Name = "pgbarGuessesPvlt";
            this.pgbarGuessesPvlt.Size = new System.Drawing.Size(134, 16);
            this.pgbarGuessesPvlt.TabIndex = 11;
            // 
            // lblGuessesLeftPvlt
            // 
            this.lblGuessesLeftPvlt.AutoSize = true;
            this.lblGuessesLeftPvlt.Location = new System.Drawing.Point(6, 100);
            this.lblGuessesLeftPvlt.Name = "lblGuessesLeftPvlt";
            this.lblGuessesLeftPvlt.Size = new System.Drawing.Size(68, 13);
            this.lblGuessesLeftPvlt.TabIndex = 10;
            this.lblGuessesLeftPvlt.Text = "Guesses left:";
            // 
            // btnGuessPvlt
            // 
            this.btnGuessPvlt.Location = new System.Drawing.Point(7, 46);
            this.btnGuessPvlt.Name = "btnGuessPvlt";
            this.btnGuessPvlt.Size = new System.Drawing.Size(136, 47);
            this.btnGuessPvlt.TabIndex = 7;
            this.btnGuessPvlt.Text = "Guess!";
            this.btnGuessPvlt.UseVisualStyleBackColor = true;
            this.btnGuessPvlt.Click += new System.EventHandler(this.btnGuessPvlt_Click);
            // 
            // lblGuessPvlt
            // 
            this.lblGuessPvlt.AutoSize = true;
            this.lblGuessPvlt.Location = new System.Drawing.Point(87, 23);
            this.lblGuessPvlt.Name = "lblGuessPvlt";
            this.lblGuessPvlt.Size = new System.Drawing.Size(54, 13);
            this.lblGuessPvlt.TabIndex = 1;
            this.lblGuessPvlt.Text = "My Guess";
            // 
            // tbxMyGuessPvlt
            // 
            this.tbxMyGuessPvlt.Location = new System.Drawing.Point(7, 20);
            this.tbxMyGuessPvlt.Name = "tbxMyGuessPvlt";
            this.tbxMyGuessPvlt.Size = new System.Drawing.Size(74, 20);
            this.tbxMyGuessPvlt.TabIndex = 0;
            // 
            // tbarDifferencePvlt
            // 
            this.tbarDifferencePvlt.Enabled = false;
            this.tbarDifferencePvlt.Location = new System.Drawing.Point(362, 32);
            this.tbarDifferencePvlt.Name = "tbarDifferencePvlt";
            this.tbarDifferencePvlt.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.tbarDifferencePvlt.Size = new System.Drawing.Size(45, 139);
            this.tbarDifferencePvlt.TabIndex = 11;
            // 
            // lblColdPvlt
            // 
            this.lblColdPvlt.AutoSize = true;
            this.lblColdPvlt.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblColdPvlt.Location = new System.Drawing.Point(362, 16);
            this.lblColdPvlt.Name = "lblColdPvlt";
            this.lblColdPvlt.Size = new System.Drawing.Size(28, 13);
            this.lblColdPvlt.TabIndex = 13;
            this.lblColdPvlt.Text = "Cold";
            // 
            // lvlWarmPvlt
            // 
            this.lvlWarmPvlt.AutoSize = true;
            this.lvlWarmPvlt.ForeColor = System.Drawing.Color.Red;
            this.lvlWarmPvlt.Location = new System.Drawing.Point(359, 171);
            this.lvlWarmPvlt.Name = "lvlWarmPvlt";
            this.lvlWarmPvlt.Size = new System.Drawing.Size(35, 13);
            this.lvlWarmPvlt.TabIndex = 14;
            this.lvlWarmPvlt.Text = "Warm";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.lblLostValuePvlt);
            this.groupBox5.Controls.Add(this.lblWonValuePvlt);
            this.groupBox5.Controls.Add(this.lblLostPvlt);
            this.groupBox5.Controls.Add(this.lblWonPvlt);
            this.groupBox5.Location = new System.Drawing.Point(12, 191);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(344, 47);
            this.groupBox5.TabIndex = 14;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Stats";
            // 
            // lblLostValuePvlt
            // 
            this.lblLostValuePvlt.AutoSize = true;
            this.lblLostValuePvlt.Location = new System.Drawing.Point(237, 20);
            this.lblLostValuePvlt.Name = "lblLostValuePvlt";
            this.lblLostValuePvlt.Size = new System.Drawing.Size(16, 13);
            this.lblLostValuePvlt.TabIndex = 13;
            this.lblLostValuePvlt.Text = "...";
            // 
            // lblWonValuePvlt
            // 
            this.lblWonValuePvlt.AutoSize = true;
            this.lblWonValuePvlt.Location = new System.Drawing.Point(98, 20);
            this.lblWonValuePvlt.Name = "lblWonValuePvlt";
            this.lblWonValuePvlt.Size = new System.Drawing.Size(16, 13);
            this.lblWonValuePvlt.TabIndex = 12;
            this.lblWonValuePvlt.Text = "...";
            // 
            // lblLostPvlt
            // 
            this.lblLostPvlt.AutoSize = true;
            this.lblLostPvlt.Location = new System.Drawing.Point(172, 20);
            this.lblLostPvlt.Name = "lblLostPvlt";
            this.lblLostPvlt.Size = new System.Drawing.Size(30, 13);
            this.lblLostPvlt.TabIndex = 11;
            this.lblLostPvlt.Text = "Lost:";
            // 
            // lblWonPvlt
            // 
            this.lblWonPvlt.AutoSize = true;
            this.lblWonPvlt.Location = new System.Drawing.Point(6, 20);
            this.lblWonPvlt.Name = "lblWonPvlt";
            this.lblWonPvlt.Size = new System.Drawing.Size(33, 13);
            this.lblWonPvlt.TabIndex = 10;
            this.lblWonPvlt.Text = "Won:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(413, 245);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.lvlWarmPvlt);
            this.Controls.Add(this.lblColdPvlt);
            this.Controls.Add(this.tbarDifferencePvlt);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Mystery Number";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbarDifferencePvlt)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblMinValuePvlt;
        private System.Windows.Forms.TextBox tbxMinValuePvlt;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox chboxExtraPvlt;
        private System.Windows.Forms.CheckBox chboxCheatPvlt;
        private System.Windows.Forms.Button btnStartPvlt;
        private System.Windows.Forms.ComboBox cmbAttemptsPvlt;
        private System.Windows.Forms.Label lblAttemptsPvlt;
        private System.Windows.Forms.Label lblMaxValuePvlt;
        private System.Windows.Forms.TextBox tbxMaxValuePvlt;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnGuessPvlt;
        private System.Windows.Forms.Label lblGuessPvlt;
        private System.Windows.Forms.TextBox tbxMyGuessPvlt;
        private System.Windows.Forms.Label lblDifferenceCheatPvlt;
        private System.Windows.Forms.Label lblNumberCheatPvlt;
        private System.Windows.Forms.Label lblPreviousGuessesPvlt;
        private System.Windows.Forms.ProgressBar pgbarGuessesPvlt;
        private System.Windows.Forms.Label lblGuessesLeftPvlt;
        private System.Windows.Forms.TrackBar tbarDifferencePvlt;
        private System.Windows.Forms.Button btnExtraExitPvlt;
        private System.Windows.Forms.Button btnExtraRestartPvlt;
        private System.Windows.Forms.Button btnExtraAboutPvlt;
        private System.Windows.Forms.Button btnExtraLocatePvlt;
        private System.Windows.Forms.Button btnExtraManualPvlt;
        private System.Windows.Forms.Label lblCheatNumber;
        private System.Windows.Forms.Label lblDifferenceValuePvlt;
        private System.Windows.Forms.Label lblPrevGuessValuePvlt;
        private System.Windows.Forms.Label lblColdPvlt;
        private System.Windows.Forms.Label lvlWarmPvlt;
        private System.Windows.Forms.Label lblGuessesLeftValuePvlt;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label lblLostValuePvlt;
        private System.Windows.Forms.Label lblWonValuePvlt;
        private System.Windows.Forms.Label lblLostPvlt;
        private System.Windows.Forms.Label lblWonPvlt;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace mysteryNumberPvlt10_9_2018
{
    public partial class Form1 : Form
    {

    #region Audio Setup
        //Audio DLL import
        [DllImport("winmm.dll")]

        //default audio handling code
        private static extern long
        mciSendString(string strCommand,
        StringBuilder strReturn,
        int iReturnLength,
        IntPtr hwndCallback);
    #endregion

    #region Form Setup
        //initializes form, fills standard values and converts to ints!
        public Form1()
        {
            InitializeComponent();
            tbxMinValuePvlt.Text = "1";
            tbxMaxValuePvlt.Text = "100";
            cmbAttemptsPvlt.Text = "10";
            minValuePvlt = Convert.ToInt32(tbxMinValuePvlt.Text);
            maxValuePvlt = Convert.ToInt32(tbxMaxValuePvlt.Text);
        }
    #endregion

    #region Variables
        //initializes variables
        int minValuePvlt = 1;
        int maxValuePvlt = 100;
        int randomNumberPvlt;
        int guessValuePvlt;
        int attemptsMaxPvlt;
        int attemptsLeftPvlt;
        int diffValuesPvlt;
        int lossValuePvlt = 0;
        int winValuePvlt = 0;
    #endregion

    #region About button
        //about button clicked fires messagebox
        private void btnExtraAboutPvlt_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This application was designed by and made possible thanks to \n \tPatrick Veltrop");
        }
    #endregion

    #region Exit button
        //closes this form when clicked
        private void btnExtraExitPvlt_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    #endregion

    #region Location button
        //shows location of the application
        private void btnExtraLocatePvlt_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Application.ExecutablePath);
        }
    #endregion

    #region Manual button
        //opens a manual PDF file
        private void btnExtraManualPvlt_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"..\..\Manual\Manual.pdf");
        }
    #endregion

    #region Generate/start button
        //generates a random number, sends values to labels and disables the textboxes
        private void btnStartPvlt_Click(object sender, EventArgs e)
        {
            try
            {
                //converts textbox input to int values
                minValuePvlt = Convert.ToInt32(tbxMinValuePvlt.Text);
                maxValuePvlt = Convert.ToInt32(tbxMaxValuePvlt.Text);

                //generates random number from min and max values
                Random randomGeneratorPvlt = new Random();
                randomNumberPvlt = randomGeneratorPvlt.Next(minValuePvlt, maxValuePvlt);

                //sets attempts and cheat label
                attemptsMaxPvlt = Convert.ToInt32(cmbAttemptsPvlt.SelectedItem);
                lblCheatNumber.Text = Convert.ToString(randomNumberPvlt);
            }

            //error messages if number cant be converted/used
            catch
            {
                //if the minimum value exceeds the maximum value
                if (minValuePvlt > maxValuePvlt)
                {
                    MessageBox.Show("The minimum value can't be bigger than the maximum value.");
                    return;
                }

                else
                {
                    MessageBox.Show("Enter numeric values.");
                    return;
                }
            }


            //disables textboxes
            tbxMinValuePvlt.Enabled = false;
            tbxMaxValuePvlt.Enabled = false;
            cmbAttemptsPvlt.Enabled = false;

            //calculates attempts left from maximum to be used later
            attemptsLeftPvlt = attemptsMaxPvlt;

            //sets progressbar values for attempts
            pgbarGuessesPvlt.Maximum = attemptsMaxPvlt;
            pgbarGuessesPvlt.Value = attemptsLeftPvlt;
        }
    #endregion

    #region Guess button
        //sends guessed number ingame to calculate if its correct
        private void btnGuessPvlt_Click(object sender, EventArgs e)
        {

            //converts entered input to ints
            try
            {
                guessValuePvlt = Convert.ToInt32(tbxMyGuessPvlt.Text);
                lblPrevGuessValuePvlt.Text = Convert.ToString(guessValuePvlt);

                //calculates the difference between the random number and guessed number, sends it to label
                diffValuesPvlt = Math.Abs(randomNumberPvlt - guessValuePvlt);
                lblDifferenceValuePvlt.Text = Convert.ToString(diffValuesPvlt);
            }

            //error message if input isnt correct
            catch
            {
                MessageBox.Show("Enter numeric values.");
                return;
            }

            //error message incase the input isnt correct
            if (guessValuePvlt < minValuePvlt || guessValuePvlt > maxValuePvlt)
            {
                MessageBox.Show("Enter a value between the minimum and maximum value.");
                return;
            }

            //sets the trackbar values and guess attempts if input is correct
            else
            {
                tbarDifferencePvlt.Maximum = maxValuePvlt;
                tbarDifferencePvlt.Value = diffValuesPvlt;

                //lowers guess attempts every click
                attemptsLeftPvlt--;
                pgbarGuessesPvlt.Value = attemptsLeftPvlt;
                lblGuessesLeftValuePvlt.Text = Convert.ToString(attemptsLeftPvlt);
            }

            //if the user guesses right
            if (guessValuePvlt == randomNumberPvlt)
            {
                ResetGame();

                //stops music so it wont overlap the new music
                StopMusic();

                //play audio
                mciSendString("open \"" + Application.StartupPath
                      + "\\..\\..\\Music\\"
                      + "Clear.mp3\" "
                      + "type mpegvideo alias MediaFile", null, 0, IntPtr.Zero);
                mciSendString("play MediaFile", null, 0, IntPtr.Zero);

                //Messagebox
                MessageBox.Show("You guessed correctly!");

                //adds one win to the stats
                winValuePvlt++;
                lblWonValuePvlt.Text = Convert.ToString(winValuePvlt);
            }

            //if the user guesses wrong
            if (attemptsLeftPvlt == 0)
            {
                ResetGame();

                //stops music so it wont overlap the new music
                StopMusic();

                //play audio
                mciSendString("open \"" + Application.StartupPath
                      + "\\..\\..\\Music\\"
                      + "GameOver.mp3\" "
                      + "type mpegvideo alias MediaFile", null, 0, IntPtr.Zero);
                mciSendString("play MediaFile", null, 0, IntPtr.Zero);

                //Messagebox
                MessageBox.Show("You failed. The random number was " + randomNumberPvlt + ".");

                //adds one loss to the stats
                lossValuePvlt++;
                lblLostValuePvlt.Text = Convert.ToString (lossValuePvlt);
            }
        }

        private static void StopMusic()
        {
            mciSendString("close MediaFile", null, 0, IntPtr.Zero);
        }
        #endregion

    #region Reset Method
        //reset method
        private void ResetGame()
        {
            //resets all text items
            tbxMinValuePvlt.Text = "1";
            tbxMaxValuePvlt.Text = "100";
            tbxMyGuessPvlt.Text = "";
            cmbAttemptsPvlt.Text = "10";
            lblGuessesLeftValuePvlt.Text = "...";
            lblPrevGuessValuePvlt.Text = "...";
            lblCheatNumber.Text = "...";
            lblDifferenceValuePvlt.Text = "...";

            //resets progressbar and trackbar
            pgbarGuessesPvlt.Value = 0;
            tbarDifferencePvlt.Value = tbarDifferencePvlt.Maximum;

            //resets trackbar
            attemptsLeftPvlt = 10;

            //enables textboxes
            tbxMinValuePvlt.Enabled = true;
            tbxMaxValuePvlt.Enabled = true;
            cmbAttemptsPvlt.Enabled = true;
            tbxMyGuessPvlt.Enabled = true;
        }
    #endregion

    #region Cheat button
        //cheat button is checked
        private void chboxCheatPvlt_CheckedChanged(object sender, EventArgs e)
        {
            //expands the forms height to show the cheat labels if the cheat button is pressed
            if (chboxCheatPvlt.Checked == true)
            {
                this.Height = this.Height + 60;
            }

            //returns the forms height to normal if cheat button is unchecked
            else
            {
                this.Height = this.Height - 60;
            }
        }
    #endregion

    #region Extra button
        //extra button is checked
        private void chboxExtraPvlt_CheckedChanged(object sender, EventArgs e)
        {
            //expands the forms width to show the extra buttons if the extra button is pressed
            if (chboxExtraPvlt.Checked == true)
            {
                this.Width = this.Width + 113;
            }

            //returns the forms width to normal if extra button is unchecked
            else
            {
                this.Width = this.Width - 113;
            }
        }
    #endregion

    #region Restart button
        //restarts the entire application
        private void btnExtraRestartPvlt_Click(object sender, EventArgs e)
        {
            ResetGame();

            //clears stats since its a full restart
            winValuePvlt = 0;
            lossValuePvlt = 0;
            lblLostValuePvlt.Text = "...";
            lblWonValuePvlt.Text = "...";
        }
    #endregion

    }
}
